README


This sample application focuses to accomplish the requirements mentioned in the brief.txt provided as an exercise.

As by the requirement the code is provided as a zip file and the app apk is attached in the mail.

The list of requirement and the respective status in terms of code are given below;

1) Requirement: show a list of transactions corresponding to an account (Android.Transactions.png) [Status: Done]

2) Requirement: Example data has been included (exercise.json) [Status: The sample is just used for reference, and not integrated with code. If needed it can be done as well. ]

3) Requirement: Pending transactions are combined with the transactions.

Bonus requirements —————————---------

4) Requirement: Group transactions by date [Status: Done]

5) Requirement: Indicate ATM Withdrawals by icon [Status: Done. The placement of icon is on the bottom right corner of the list as the VD is not provided for this case.]

6) Requirement: Tapping on an ATM withdrawal row will show the location of the ATM on a map [Status: Done]

7) Requirement: Implement an algorithm that will give the user a projected spend over the next two weeks [Status: Done. There is a option menu available in the top right corner of the MainActivity action bar. Clicking on this menu calculates and provides the average fortnight spending. The wordings is not to show for the next weeks as the transactions in the json provided are very old.]

8) Requirement: Dynamically pull down data from the given url [Status: Done. The data is pulled from this url if there is internet connection. Pull to refresh is also given]

9) Requirement: Kotlin coding [ Status: Done. There is a Java file also there in between to demonstrate the intergration of Java and Kotlin]
Improvements

1) There is no focus on the unit testing as it is not part of the requirement, but if required I can add them.

2) Integration of Dagger2 to make the dependency injection.