package cba.com.cbasample.model

import java.io.Serializable
import java.util.ArrayList

/*
 * Models used by the txn json response
 */

class TxnData {
    var account: TxnAccountSummary? = null
    var transactions: ArrayList<CbaTxn>? = null
    var pending: ArrayList<CbaTxn>? = null
    var atms: ArrayList<TxnAtmData>? = null
}

class TxnAtmLocation : Serializable {
    var lat: String? = null
    var lng: String? = null
}

class TxnAtmData : Serializable {
    var id: String? = null
    var name: String? = null
    var address: String? = null
    var location: TxnAtmLocation? = null
}

class TxnAccountSummary : Serializable {
    var accountName: String? = null
    var accountNumber: String? = null
    var available: Float = 0.toFloat()
    var balance: Float = 0.toFloat()
}

class CbaTxn : Serializable {
    var id: String? = null
    var effectiveDate: String? = null
    var description: String? = null
    var amount: Float = 0.toFloat()
    var atmId: String? = null
}