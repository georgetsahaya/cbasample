package cba.com.cbasample.util

import android.content.Context
import android.text.TextUtils
import cba.com.cbasample.*
import cba.com.cbasample.model.CbaTxn
import cba.com.cbasample.model.TxnData
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/*
 * Utility methods with helper functions
 */

class Util {
    companion object {
        fun calculateFortnightExpense(txns: ArrayList<CbaTxn>): Float {
            val expenseTrackerMap = HashMap<Int, Float>()
            if (txns != null && txns.size > 1) {
                for (i in 1 until txns.size) {
                    val cbaTxn = txns.get(i)

                    if (cbaTxn != null && !TextUtils.isEmpty(cbaTxn!!.effectiveDate)  //if date is available
                            && cbaTxn!!.amount < 0) {  //negative values to track the expenses
                        val date = Util.getDate(cbaTxn!!.effectiveDate)
                        val calendar = Calendar.getInstance()
                        calendar.time = date!!
                        val weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR)
                        //Make it even so that 27 fortnights are counted
                        val fortnightOfYear = (weekOfYear + weekOfYear % 2) / 2
                        val expense = expenseTrackerMap[fortnightOfYear]
                        if (expense == null) {
                            expenseTrackerMap[fortnightOfYear] = cbaTxn!!.amount
                        } else {
                            expenseTrackerMap[fortnightOfYear] = expense + cbaTxn!!.amount
                        }
                    }
                }
            }

            if (!expenseTrackerMap.isEmpty()) {
                val entries = expenseTrackerMap.entries
                var total: Float = 0f
                for ((_, value) in entries) {
                    total += value
                }

                return total / entries.size.toFloat()
            }

            return 0f
        }

        fun getDate(str: String?): Date? {
            val format = SimpleDateFormat(DATE_FORMAT)
            try {
                return format.parse(str)
            } catch (e: Exception) {
                return null
            }
        }

        fun getTxnList(txnData: TxnData): ArrayList<CbaTxn> {
            val listTxns: ArrayList<CbaTxn> = ArrayList<CbaTxn>()
            txnData?.let {
                listTxns.addAll(txnData.transactions as ArrayList<CbaTxn>)

                txnData.pending?.apply {
                    val loopCount = txnData.pending!!.size
                    for (i in 0 until loopCount) {
                        val txn = txnData.pending!!.get(i)
                        txn.description = PENDING_PREFIX + txn.description
                        listTxns.add(txn)
                    }
                }
            }

            Collections.sort(listTxns, Comparator<CbaTxn> { o1, o2 ->
                val d1 = getDate(o1.effectiveDate)
                val d2 = getDate(o2.effectiveDate)

                if (d2 != null && d1 != null) {
                    d2.compareTo(d1)
                } else {
                    return@Comparator -1
                }
            })

            return listTxns
        }
    }
}