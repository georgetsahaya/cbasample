package cba.com.cbasample.util;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import cba.com.cbasample.R;
import cba.com.cbasample.model.CbaTxn;

import static cba.com.cbasample.CbaConstantsKt.DATE_FORMAT;
import static cba.com.cbasample.CbaConstantsKt.DATE_HEADER;
import static cba.com.cbasample.CbaConstantsKt.MSEC_IN_A_DAY;

/*
 * Java utility methods to demonstate the use of Java legacy libraries
 */

public class UtilJava {
    public static String getDateDifferenceFlag(Context context, String dateArg) {
        String toRet = "";

        Date today = new Date();
        Date date = Util.Companion.getDate(dateArg);

        int difference = today.compareTo(date);

        int diffInDays = (int) ((today.getTime() - date.getTime())/ MSEC_IN_A_DAY);

        if (difference == 0)
            return context.getString(R.string.today);
        else
            return context.getString(R.string.days_ago, diffInDays) ;
    }

    public static void addDateField(Context context, ArrayList list) {
        CbaTxn txn = new CbaTxn();
        txn.setId(DATE_HEADER);
        txn.setEffectiveDate(((CbaTxn)list.get(1)).getEffectiveDate());
        txn.setDescription(getDateDifferenceFlag(context, txn.getEffectiveDate()));

        list.add(1, txn);
        int j = 2 ;

        for (int i = 3;i < list.size();i++) { //First two items are summary and the first date header - hence ignore them
            CbaTxn txn1 = (CbaTxn) list.get(j);
            CbaTxn txn2 = (CbaTxn) list.get(i);

            if (!txn1.getEffectiveDate().equals(txn2.getEffectiveDate())) {
                CbaTxn txn3 = new CbaTxn();
                txn3.setId(DATE_HEADER);
                txn3.setEffectiveDate(txn2.getEffectiveDate());
                txn3.setDescription(getDateDifferenceFlag(context, txn3.getEffectiveDate()));

                list.add(i, txn3);

                j = i ;
            }
        }
    }
}
