package cba.com.cbasample.repo

import cba.com.cbasample.DATE_FORMAT
import cba.com.cbasample.PENDING_PREFIX
import cba.com.cbasample.api.CbaTxnApi
import cba.com.cbasample.model.CbaTxn
import cba.com.cbasample.model.TxnData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

/*
 * Repo to call the API to fetch the data.
 * The caching can be managed from here. No cache implementation is done yet
 */

class CbaTxnRepo {
    val cbaTxnApi by lazy {
        CbaTxnApi.create()
    }
    var disposable: Disposable? = null

    fun getCbaTxns(): Observable<TxnData> {
        return cbaTxnApi.getCbaTxnData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }



}