package cba.com.cbasample

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import cba.com.cbasample.adapter.TxnRecyclerAdapter
import cba.com.cbasample.model.CbaTxn
import cba.com.cbasample.util.Util
import cba.com.cbasample.util.UtilJava
import cba.com.cbasample.viewModel.TxnListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    private lateinit var txnListViewModel: TxnListViewModel
    internal lateinit var txnAdapter: TxnRecyclerAdapter
    internal var txns = ArrayList<CbaTxn>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.apply {
            setIcon(R.drawable.icon_action_logo)
            setTitle(R.string.app_main_title)
        }

        txnListViewModel = ViewModelProviders.of(this).get(TxnListViewModel::class.java)
        getTxnDataAndShow(false)
        
        initialiseSwipeLayouListener()
    }

    private fun initialiseSwipeLayouListener() {
        swiperefresh.setOnRefreshListener(object:SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                getTxnDataAndShow(true)
                swiperefresh.isRefreshing = false
            }
        })
    }

    private fun getTxnDataAndShow(isRefresh: Boolean) {
        swiperefresh.isRefreshing = true

        txnListViewModel.getCbaTxnData(isRefresh).observe(this, Observer {
            if (it != null) {
                txns = Util.getTxnList(it)
                txns.add(0, CbaTxn ())

                UtilJava.addDateField(this, txns)

                txnAdapter = TxnRecyclerAdapter(getApplicationContext(), it, txns, it.atms!!)

                val mLayoutManager = LinearLayoutManager(this@MainActivity)
                recycler_view.setLayoutManager(mLayoutManager)
                recycler_view.setItemAnimator(DefaultItemAnimator())
                recycler_view.setAdapter(txnAdapter)

                txnAdapter.notifyDataSetChanged()

            } else {
                Toast.makeText(applicationContext, R.string.txn_load_error, Toast.LENGTH_SHORT).show()
            }
            swiperefresh.isRefreshing = false
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.show_expense -> {
                showFortnightExpenseAverage(Util.calculateFortnightExpense(txns))
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showFortnightExpenseAverage(expenses: Float) {
        if (expenses == 0f) {
            Toast.makeText(applicationContext, "Error in receiving data", Toast.LENGTH_SHORT).show()
            return  //Ignore silently
        }

        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle(R.string.fortnight_expense_title)
        alertDialog.setMessage(getString(R.string.fortnight_expense_text, Math.abs(expenses)))
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.string_ok)) {
            dialog, which -> dialog.dismiss()
        }
        alertDialog.show()
    }


}
