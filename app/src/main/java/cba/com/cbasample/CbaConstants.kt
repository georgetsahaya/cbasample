package cba.com.cbasample

const val TXN_URL = "https://www.dropbox.com/s/tewg9b71x0wrou9/data.json?dl=1"
const val JSONCONTENTFILE = "exercise.json"
const val DATEFLAG = "DATE"
const val DATE_HEADER = "HEADERDATE"
const val DATE_FORMAT = "dd/MM/yyyy"
const val MSEC_IN_A_DAY = (1000 * 60 * 60 * 24).toLong()
const val PENDING_PREFIX = "PENDING: "
const val ZOOM_LEVEL = 14f
const val LAT = "LAT"
const val LNG = "LNG"