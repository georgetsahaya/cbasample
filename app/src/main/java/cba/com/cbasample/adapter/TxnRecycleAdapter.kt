package cba.com.cbasample.adapter

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cba.com.cbasample.DATE_HEADER
import cba.com.cbasample.PENDING_PREFIX
import cba.com.cbasample.R
import cba.com.cbasample.model.CbaTxn
import cba.com.cbasample.model.TxnAtmData
import cba.com.cbasample.model.TxnData
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

const val SUMMARY = 0
const val HEADER_DATE = 1
const val TXN = 2
const val ATM_TXN = 3

/*
 * Adapter to load the txn list
 */
class TxnRecyclerAdapter(
        val context: Context,
        val txnData: TxnData,
        val txns: ArrayList<CbaTxn>,
        val atms: ArrayList<TxnAtmData>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    internal lateinit var inflater: LayoutInflater
    internal var currencyFormatter = NumberFormat.getCurrencyInstance() as DecimalFormat

    init {
        currencyFormatter.negativePrefix = "-" + "$"
        currencyFormatter.negativeSuffix = ""

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    fun getItem(position: Int): CbaTxn {
        return txns[position]
    }

    override fun getItemViewType(position: Int): Int {
        if (position == SUMMARY) {
            return SUMMARY
        } else {
            val txn = getItem(position)

            if (DATE_HEADER.equals(txn.id)) return HEADER_DATE

            txn.atmId?.let {
                if (it.length > 0) return ATM_TXN
            }

            return TXN
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            SUMMARY -> TxnHeaderViewHolder(inflater.inflate(R.layout.txn_header, parent, false))
            HEADER_DATE -> TxnDateViewHolder(inflater.inflate(R.layout.time_header, parent, false))
            ATM_TXN -> {
                val txnAtmViewHolder = TxnAtmViewHolder(context, inflater.inflate(R.layout.txn_item, parent, false))
                txnAtmViewHolder.atms = atms //atm info to refer if the atm item is clicked
                txnAtmViewHolder
            }
            else -> TxnViewHolder(context, inflater.inflate(R.layout.txn_item, parent, false))
        }
    }

    override fun getItemCount() = txns.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val txn = getItem(position)

        when (getItemViewType(position)) {
            SUMMARY -> {
                val headerHolder = viewHolder as TxnHeaderViewHolder
                txnData.account?.let {
                    headerHolder.accName.setText(it.accountName)
                    headerHolder.accNumber.setText(it.accountNumber)
                    headerHolder.availableAmt.setText(currencyFormatter.format(it.available))
                    headerHolder.balanceAmt.setText(currencyFormatter.format(it.balance))
                }
            }

            HEADER_DATE -> {
                val dateHolder = viewHolder as TxnDateViewHolder
                dateHolder.date.text = txn.effectiveDate
                dateHolder.dateDesc.text = txn.description
            }

            ATM_TXN -> {
                val txnAtmHolder = viewHolder as TxnViewHolder
                (txnAtmHolder as TxnAtmViewHolder).txn = txn
                txnAtmHolder.itemDesc.text = Html.fromHtml(txn.description)
                txnAtmHolder.itemAmt.text = currencyFormatter.format(txn.amount)
                txnAtmHolder.atmIcon.visibility = View.VISIBLE
            }

            TXN -> {
                val txnHolder = viewHolder as TxnViewHolder

                txn.description?.let {
                    val ss = SpannableString(Html.fromHtml(it))
                    if (it.startsWith(PENDING_PREFIX)) {
                        ss.setSpan(StyleSpan(Typeface.BOLD), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                    txnHolder.itemDesc.text = ss
                    txnHolder.itemAmt.text = currencyFormatter.format(txn.amount)
                    txnHolder.atmIcon.visibility = View.INVISIBLE
                }
            }
        }
    }
}