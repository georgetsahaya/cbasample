package cba.com.cbasample.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import cba.com.cbasample.LAT
import cba.com.cbasample.LNG
import cba.com.cbasample.MapsActivity
import cba.com.cbasample.R
import cba.com.cbasample.model.CbaTxn
import cba.com.cbasample.model.TxnAtmData
import cba.com.cbasample.model.TxnAtmLocation
import java.util.ArrayList

/*
 * Viewholders used by the recycler adapter
 */
class TxnHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var accName: TextView
    var accNumber: TextView
    var availableAmt: TextView
    var balanceAmt: TextView

    init {
        accName = view.findViewById(R.id.account_name) as TextView
        accNumber = view.findViewById(R.id.acc_no) as TextView
        availableAmt = view.findViewById(R.id.available_amt) as TextView
        balanceAmt = view.findViewById(R.id.balance_amt) as TextView

    }
}

class TxnDateViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var date: TextView
    var dateDesc: TextView

    init {
        date = view.findViewById(R.id.item_date) as TextView
        dateDesc = view.findViewById(R.id.item_date_desc) as TextView
    }
}

open class TxnViewHolder(var context: Context, view: View) : RecyclerView.ViewHolder(view) {
    var itemDesc: TextView
    var itemAmt: TextView
    var atmIcon: ImageView

    init {
        itemDesc = view.findViewById(R.id.item_desc) as TextView
        itemAmt = view.findViewById(R.id.item_amt) as TextView
        atmIcon = view.findViewById(R.id.atm_icon) as ImageView
    }
}

internal class TxnAtmViewHolder(context: Context, view: View) : TxnViewHolder(context, view), View.OnClickListener {
    var txn: CbaTxn? = null
    var atms: ArrayList<TxnAtmData>? = null

    val atmData: TxnAtmLocation
        get() {
            for (data in atms!!) {
                if (data.id.equals(txn!!.atmId) && data.location != null) {
                    return data.location as TxnAtmLocation
                }
            }

            return TxnAtmLocation()
        }

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        val intent = Intent(context, MapsActivity::class.java)
        intent.putExtra(LAT, atmData.lat?.toFloat())
        intent.putExtra(LNG, atmData.lng?.toFloat())
        context.startActivity(intent)
    }


}
