package cba.com.cbasample.api

import cba.com.cbasample.TXN_URL
import cba.com.cbasample.model.TxnData
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/*
 * Api to call the txn web API to get the data
 */
interface CbaTxnApi {
    @GET(TXN_URL)
    fun getCbaTxnData(): Observable<TxnData>

    companion object {
        private lateinit var retrofit: Retrofit

        fun create(): CbaTxnApi {
            if (!::retrofit.isInitialized) {
                retrofit = Retrofit.Builder()
                        .addCallAdapterFactory(
                                RxJava2CallAdapterFactory.create())
                        .addConverterFactory(
                                GsonConverterFactory.create())
                        .baseUrl("https://en.wikipedia.org/w/")
                        .build()
            }

            return retrofit.create(CbaTxnApi::class.java)
        }
    }

}