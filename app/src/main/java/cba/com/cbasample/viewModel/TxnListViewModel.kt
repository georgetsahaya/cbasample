package cba.com.cbasample.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import cba.com.cbasample.DATE_FORMAT
import cba.com.cbasample.PENDING_PREFIX
import cba.com.cbasample.model.CbaTxn
import cba.com.cbasample.model.TxnAtmData
import cba.com.cbasample.model.TxnData
import cba.com.cbasample.repo.CbaTxnRepo
import java.text.SimpleDateFormat
import java.util.*

/*
 * View model used by main activity, mainly to get the txn data
 */

class TxnListViewModel : ViewModel() {
    private lateinit var txnLiveData: MutableLiveData<TxnData>
    private lateinit var txnData: TxnData

    fun getCbaTxnData(isRefresh: Boolean): LiveData<TxnData> {
        if (!::txnLiveData.isInitialized) {
            txnLiveData = MutableLiveData()
            if (!isRefresh) loadTxnData()
        }

        if (isRefresh) loadTxnData()

        return txnLiveData;
    }

    private fun loadTxnData() {
        CbaTxnRepo().getCbaTxns().subscribe({
            it?.let {
                txnData = it
                txnLiveData.postValue(it)
            }
        }, {
            txnLiveData.postValue(null)
        })
    }




}